/*
 *   Copyright (c) 2020, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *   Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *   Version 2.0 (the "License"); you may not use this file except
 *   in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied. See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */


package org.wso2.carbon.device.mgt.core.search.mgt;

public class Constants {

    public static final String GENERAL = "GENERAL";
    public static final String PROP_AND = "PROP_AND";
    public static final String PROP_OR = "PROP_OR";
    public static final String LOCATION = "LOCATION";

    public static final String ANY_DEVICE_PERMISSION = "/device-mgt/devices/any-device";
    public static final String UI_EXECUTE = "ui.execute";
}
